import Vue from 'vue'
import firebase from 'firebase'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

// Import Layouts
import Navbar from '../layouts/Navbar'
//import Navbar2 from '../layouts/Navbar2'

// Import Pages
import Home from '../views/Home'
//import Quiz from '../views/Quiz'
//import Courses from '../views/Courses'
//import Modules from '../views/Modules'
//import Exercise from '../views/Exercise'
import FaqShow from '../views/Faqs/Show'
import NotFound from '../views/NotFound'
//import FaqIndex from '../views/Faqs/Index'

// Terms & Policy
import Legalese from '../views/Legalese'

// Authentification
import Login from '../views/auth/Login'
import Verify from '../views/auth/Verify'
import Profile from '../views/auth/Profile'
import Register from '../views/auth/Register'
import Settings from '../views/auth/Settings'
import ForgotPassword from '../views/auth/ForgotPassword'

// Modules
import Module_0_01 from '../views/Modules/Module_0_01'
import Module_0_02 from '../views/Modules/Module_0_02'
import Module_1_01 from '../views/Modules/Module_1_01'
import Module_2_01 from '../views/Modules/Module_2_01'

// All routes.
const routes = [
  {
    name: 'not-found',
    path: '*',
    components: {
      navbar: Navbar,
      default: NotFound
    }
  },
  {
    name: 'home',
    path: '/',
    components: {
      navbar: Navbar,
      default: Home
    }
  },
  /*{
    name: 'courses',
    path: '/courses',
    components: {
      navbar: Navbar,
      default: Courses
    }
  },*/
  /*{
    name: 'modules',
    path: '/modules',
    components: {
      navbar: Navbar,
      default: Modules
    }
  },*/
  {
    name: 'module-0-01',
    path: '/modules/0/episodes/1',
    components: {
      navbar: Navbar,
      default: Module_0_01
    }
  },
  {
    name: 'module-0-02',
    path: '/modules/0/episodes/2',
    components: {
      navbar: Navbar,
      default: Module_0_02
    }
  },
  {
    name: 'module-1-01',
    path: '/modules/1/episodes/1',
    components: {
      navbar: Navbar,
      default: Module_1_01
    }
  },
  {
    name: 'module-2-01',
    path: '/modules/2/episodes/1',
    components: {
      navbar: Navbar,
      default: Module_2_01
    }
  },
  {
    name: 'faqs.index',
    path: '/faqs',
    components: {
      navbar: Navbar,
      default: FaqShow
    }
  },
  /*{
    name: 'faqs.show',
    path: '/faqs/:tag/:slug',
    components: {
      navbar: Navbar,
      default: FaqShow
    }
  },*/
  {
    name: 'legal',
    path: '/legal',
    components: {
      navbar: Navbar,
      default: Legalese
    }
  },
  /*{
    name: 'quiz',
    path: '/quiz',
    components: {
      navbar: Navbar2,
      default: Quiz
    }
  },*/
  /*{
    name: 'exercise',
    path: '/exercise/:slug',
    component: Exercise
  },*/
  {
    name: 'register',
    path: '/register',
    components: {
      navbar: Navbar,
      default: Register
    },
    meta: { requiresAuth: false }
  },
  {
    name: 'login',
    path: '/login',
    components: {
      navbar: Navbar,
      default: Login
    },
    meta: { requiresAuth: false }
  },
  {
    name: 'forgot',
    path: '/forgot-password',
    components: {
      navbar: Navbar,
      default: ForgotPassword
    },
    meta: { requiresAuth: false }
  },
  {
    name: 'verify',
    path: '/verify-email',
    components: {
      navbar: Navbar,
      default: Verify
    },
    meta: { requiresAuth: true }
  },
  {
    name: 'settings',
    path: '/settings',
    components: {
      navbar: Navbar,
      default: Settings
    },
    meta: { requiresAuth: true }
  },
  {
    name: 'profile',
    path: '/my-profile',
    components: {
      navbar: Navbar,
      default: Profile
    },
    meta: { requiresAuth: true }
  }
]

/**
 * Create and export routes.
 */
const router = new VueRouter({
  mode: 'history',
  linkActiveClass: '',
  linkExactActiveClass: 'font-airbnb-cereal-app-bold text-blue-default hover:opacity-100',
  routes
})

router.beforeEach((to, from, next) => {
  const authenticated = firebase.auth().currentUser
  switch (to.name) {
    case 'login':
      if (authenticated) next('/')
      else next()
      break

    case 'register':
      if (authenticated) next('/')
      else next()
      break

    case 'verify':
      if (authenticated && authenticated.emailVerified) next('/')
      else if (authenticated && !authenticated.emailVerified) next()
      else next('/login')
      break

    case 'settings':
      if (authenticated && authenticated.emailVerified) next()
      else if (authenticated && !authenticated.emailVerified) next({ name: 'verify' })
      else next('/login')
      break

    case 'profile':
      if (authenticated && authenticated.emailVerified) next()
      else if (authenticated && !authenticated.emailVerified) next({ name: 'verify' })
      else next('/login')
      break
    default:
      next()
      break
  }
})

export default router
