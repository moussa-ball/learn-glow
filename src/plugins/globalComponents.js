import Nl2br from 'vue-nl2br'
import VueCarousel from 'vue-carousel'
var VueScrollTo = require('vue-scrollto')
import * as components from '@/components'
import Transitions from 'vue2-transitions'

import VuePlyr from 'vue-plyr'
import 'vue-plyr/dist/vue-plyr.css'

import VueMeta from 'vue-meta'
import VueYoutube from 'vue-youtube'

import { Plugin } from 'vue2-storage'

export default {
  install(Vue) {
    // Import All vue components in components folder.
    Object.entries(components).forEach(([name, component]) => {
      Vue.component(name, component)
    })

    // Install Vue Meta.
    Vue.use(VueMeta, {
      refreshOnceOnNavigation: true
    })

    // Import components
    Vue.component('nl2br', Nl2br)
    Vue.use(VueCarousel)
    Vue.use(VueScrollTo)
    Vue.use(Transitions)
    Vue.use(VueYoutube)
    Vue.use(Plugin, {
      prefix: 'glow_',
      driver: 'local',
      ttl: 60 * 60 * 24 * 1000, // 24 часа
      replacer: (key, value) => value
    })

    Vue.use(VuePlyr, {
      plyr: {}
    })
  }
}
