import firebase from 'firebase'
import router from '../../router/index'

const state = {
  user: null
}

const getters = {
  user() {
    return state.user
  }
}

const mutations = {
  SET_USER(state, { user }) {
    state.user = user
  },
  LOG_IN(state, { user }) {
    state.user = user
  },
  LOG_OUT(state) {
    state.user = null
  }
}

const actions = {
  user(context, user) {
    context.commit('SET_USER', { user })
  },
  async resend() {
    const user = firebase.auth().currentUser
    user
      .sendEmailVerification({
        url: window.location.origin,
        handleCodeInApp: true
      })
      .then(() => {
        this._vm.Notify('A verification email has been sent to you.', 'success')
        router.push({ name: 'verify' })
      })
      .catch(error => {
        this._vm.Notify(error.message)
      })
  },
  async login(context, form) {
    form.processing = true
    await firebase
      .auth()
      .setPersistence(form.remember ? firebase.auth.Auth.Persistence.LOCAL : firebase.auth.Auth.Persistence.SESSION)
      .then(() => {
        firebase
          .auth()
          .signInWithEmailAndPassword(form.email, form.password)
          .then(userCredential => {
            var user = userCredential.user
            context.commit('LOG_IN', { user })
            router.push('/')
          })
          .catch(error => {
            this._vm.Notify(error.message)
            form.processing = false
          })
      })
      .catch(error => {
        this._vm.Notify(error.message)
        form.processing = false
      })
  },
  async register(context, form) {
    form.processing = true
    await firebase
      .auth()
      .createUserWithEmailAndPassword(form.email, form.password)
      .then(response => {
        /**
         * Get the new user and send an email verification.
         */
        const user = response.user
        user
          .sendEmailVerification({
            url: window.location.origin,
            handleCodeInApp: true
          })
          .then(() => {})
          .catch(error => {
            this._vm.Notify(error.message)
          })

        /**
         * Update the user data.
         */
        user
          .updateProfile({ displayName: form.username })
          .then(() => {
            firebase
              .database()
              .ref('users/' + user.uid)
              .set({ username: user.displayName })
              .then(() => {
                context.commit('SET_USER', { user })
                this._vm.Notify("Welcome to Glow's online course :-)  An email has been sent to you !", 'success')
                router.push({ name: 'verify' })
              })
              .catch(error => {
                this.Notify(error.message)
              })
          })
          .catch(error => {
            this._vm.Notify(error.message)
            form.processing = false
          })
      })
      .catch(error => {
        this._vm.Notify(error.message)
        form.processing = false
      })
  },
  async logout(context) {
    await firebase
      .auth()
      .signOut()
      .then(() => {
        context.commit('LOG_OUT')
        router.push('/login')
      })
      .catch(error => {
        this._vm.Notify(error.message)
      })
  },
  async reset(context, form) {
    let self = this
    form.processing = true
    firebase
      .auth()
      .sendPasswordResetEmail(form.email, {
        url: window.location.origin + '/login',
        handleCodeInApp: true
      })
      .then(() => {
        self._vm.Notify('We have e-mailed your password reset link.', 'success')
        form.processing = false
      })
      .catch(error => {
        self._vm.Notify(error.message)
        form.processing = false
      })
  },
  async update(context, form) {
    let self = this
    const user = firebase.auth().currentUser
    await user
      .updateProfile({
        displayName: form.username,
        photoURL: form.profile_picture
      })
      .then(() => {
        self._vm.Notify('Your profile information has been updated.', 'success')
      })
      .catch(error => {
        self._vm.Notify(error.message)
      })
  },
  delete() {
    const self = this
    const user = firebase.auth().currentUser
    if (confirm('Are you sure you want to delete your account?')) {
      // Delete user account.
      user
        .delete()
        .then(() => {
          window.location.href = '/login'
        })
        .catch(error => {
          self._vm.Notify(error.message)
        })
    }
  }
}

export default {
  strict: true,
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
