const state = {}
const getters = {}
const mutations = {}
const actions = {}

export default {
  strict: true,
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
