const MonacoWebpackPlugin = require('monaco-editor-webpack-plugin')

module.exports = {
  publicPath: '/',
  devServer: {
    host: 'localhost'
  },
  chainWebpack: config => {
    // Compile all codes in one bundle.
    config.optimization.splitChunks().clear()
    config.module
      .rule('css')
      .oneOf('vue')
      .uses.delete('extract-css-loader')

    config.module
      .rule('vue')
      .use('vue-loader')
      .loader('vue-loader')
      .tap(options => {
        options['transformAssetUrls'] = {
          // [...]
          'b-embed': ['src', 'poster'],
          source: 'src',
          track: 'src'
        }
        return options
      })

    // adjusting the media rule to include vtt files
    config.module.rule('media').test(/\.(vtt|mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/)
  },
  css: {
    extract: false
  },
  configureWebpack: {
    output: {
      filename: 'app.js'
    },
    plugins: [new MonacoWebpackPlugin()],
    optimization: {
      splitChunks: false
    }
  }
}
